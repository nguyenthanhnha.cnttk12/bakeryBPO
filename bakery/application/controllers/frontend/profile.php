<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author FitDNN
 */
class Profile extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->_is_user();
		$this->page_title = "Trang cá nhân";
		$this->load->model('admin/users_model');
		$this->load->helper(array('form', 'html', 'file', 'path'));
		$this->load->library('form_validation');
	}
	public function index() {
		$id = $this->session->userdata('user_info')['id'];
		$data['data'] = $this->users_model->get_profile_frontend_id($id);
		$this->_renderFrontLayout('profile/index', $data);
	}

	public function changepassword() {

		$id = $this->session->userdata('user_info')['id'];
		var_dump($id);
		if ($id != null) {
			if ($this->users_model->editUser($id)) {
				$this->session->set_flashdata('msg', 'Thông tin đã được cập nhật');
				redirect(site_url('profile'));
			} else {
				$this->session->set_flashdata('msg', 'Thông tin cập nhật thất bại');
				redirect(site_url('profile'));
			}

		}redirect(site_url('profile'));

	}
	public function update() {

		$id = $this->input->post('usid');
		if ($id != null) {
			if ($this->users_model->editUserInfomation($id)) {
				$this->session->set_flashdata('msg', 'Thông tin đã được cập nhật');
				redirect(site_url('profile'));
			} else {
				$this->session->set_flashdata('msg', 'Thông tin cập nhật thất bại');
				redirect(site_url('profile'));
			}

		}redirect(site_url('profile'));

	}

}
