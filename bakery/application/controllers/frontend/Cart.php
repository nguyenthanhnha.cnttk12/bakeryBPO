<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Page
 *
 * @author FitDNN
 */
class Cart extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->_is_user();
		$this->menu = 'cart';
		$this->load->model('admin/product_model');
		$this->load->helper('form');
		$this->page_title = "Giỏ hàng";
	}

	public function index() {
		$data = [];
		$this->_renderFrontLayout('cart/index', $data);
	}

	public function update() {
		$rowid = $this->input->post('rowid');
		$qty = $this->input->post('qty');
		$data = array(
			'rowid' => $rowid,
			'qty' => $qty,
		);
		// die(json_encode($this->cart->contents()));
		//$this->cart->update($data);
		if (!$this->cart->update($data)) {
			die(json_encode(['error' => 'Có lỗi xảy ra']));
		} else {
			if ($qty > 0) {
				$cart = $this->cart->contents();
				$current_item = $cart[$rowid];
				$item_subtotal = number_format($current_item['subtotal'], 0, ',', '.') . " ₫";
			} else {
				$item_subtotal = 0;
			}
			$total = number_format($this->cart->total(), 0, ',', '.') . " ₫";
			die(json_encode(['html' => 'Quý khách hủy thành công', 'qty' => $this->cart->total_items(), 'subtotal' => $item_subtotal, 'total' => $total]));
		}
	}

	public function checkout() {
		$this->page_title = "Thanh toán";
		$this->carabiner->css('js/plugins/icheck/skins/square/orange.css');
		$this->carabiner->js('js/plugins/icheck/icheck.min.js');
		$this->carabiner->js('js/validator.js');
		$this->carabiner->js('js/checkout.js');
		$this->_createOrder();
		die();
		$data = [];
		$this->_renderFrontLayout('cart/checkout', $data);
	}

	public function _createOrder() {
		$order = [];
		$order['user_id'] = $this->session->userdata('user_info')['id'];
		$order['order_date'] = date('Y-m-d H:i:s');
		$order['payment_method'] = 'cod';
		$order['status'] = 'pending';
		$order['order_code'] = 'GB' . time();
		$order_error_msg = CREATE_ORDER_ERROR_MSG;
		if (!$this->db->insert('orders', $order)) {
			show_error($order_error_msg, 500, ERROR_HEADING);
			die();
		}
		$order_id = $this->db->insert_id();
		$order_details = [];
		foreach ($this->cart->contents() as $items) {
			$order_item = array('order_id' => $order_id,
				'product_id' => $items['id'],
				'qty' => $items['qty'],
				'price' => $items['price'],
				'giavi' => $items['giavi'],
			);
			$order_details[] = $order_item;
		}
		$sub_product = [];

		if (!$this->db->insert_batch('order_detail', $order_details)) {
			$this->db->delete('orders', ['id' => $order_id]);
			show_error($order_error_msg, 500, ERROR_HEADING);
			die();
		}
		$this->db->select('u.*,ud.address');
		$this->db->from('users u');
		$this->db->join('user_address ud', 'ud.user_id = u.id','left');
		$this->db->where(array('u.id'=>$this->sesson->userdata('user_info')['id'])));

		$query = $this->db->get()
		$user = $query->row();
		$address = array('name' => , $user->name 'phone' => $user->phone, 'address' => $user->address);
		$shipping = array('order_id' => $order_id, 'address' => serialize($address));
		if (!$this->db->insert('shipping', $shipping)) {
			$this->db->delete('orders', ['id' => $order_id]);
			$this->db->delete('order_detail', ['order_id' => $order_id]);
			show_error($order_error_msg, 500, ERROR_HEADING);
			die();
		}

		//Send notification email for admin
		$data = array('order' => $order, 'cart' => $this->cart->contents(), 'address' => $address);
		$message = $this->load->view('email_template/order_notification', $data, TRUE);
//        die($message);
		$this->_sendOrderEmail($message, $order['order_code']);
		$this->cart->destroy();
		redirect(site_url('thankyou'));
	}

	public function _sendOrderEmail($message = '', $order_code) {
		$this->load->library('email');
		$this->email->from('nguyenthanhnha.cnttk12@gmail.com', 'bakeryBPO');
		$this->email->to('hodacquyenpx@gmail.com');
		$this->email->subject('Thông báo đơn hàng ' . $order_code);

		$this->email->message($message);

		$this->email->send();
	}

	public function addgiavi() {
		$rowid = $this->input->post('rowid');
		$giavi = $this->input->post('giavi');
		$data = array(
			'rowid' => $rowid,
			'giavi' => $giavi,
		);
		// if ($this->session->userdata('order')) {
		// 	$arr = $this->session->userdata('order')[$rowid];
		// 	$arr .= $giavi . ",";
		// 	$data_subproduct = array(
		// 		$rowid => $arr,
		// 	);
		// } else {
		// 	$data_subproduct = array(
		// 		$rowid => $giavi,
		// 	);
		// }
		// $this->session->set_userdata('order');
		if (!$this->cart->update($data)) {
			die(json_encode(['error' => 'Có lỗi xảy ra']));
		} else {
			die(json_encode(['giavi' => $giavi]));
		}

	}
}
