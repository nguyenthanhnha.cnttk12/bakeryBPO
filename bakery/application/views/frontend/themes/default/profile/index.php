<div class="bx-wrapper">
            <div class="slider">
               <ul class="bxslider">
                <li>
                    <img src="<?php echo site_url(); ?>assets/bxslider/images/bread-1.jpg" alt="" />

                </li>
                <li>
                    <img src="<?php echo site_url(); ?>assets/bxslider/images/bread-2.jpg" alt="" />

                </li>
                <li>
                    <img src="<?php echo site_url(); ?>assets/bxslider/images/bread-3.jpg" alt="" />

                </li>
                <li>
                    <img src="<?php echo site_url(); ?>assets/bxslider/images/bread-1.jpg" alt="" />

                </li>
                <li>
                    <img src="<?php echo site_url(); ?>assets/bxslider/images/bread-2.jpg" alt="" />

                </li>
            </ul>
            </div>

    </div>
    <div class="dish-list">
        <div class="inner">
            <div class="dish">
                <img class="dish-img" src="<?php echo site_url(); ?>assets/images/common/bread-list-1.jpg" alt="">
            </div>
            <div class="dish">
                <img class="dish-img" src="<?php echo site_url(); ?>assets/images/common/bread-list-2.jpg" alt="">
            </div>
            <div class="dish">
                <img class="dish-img" src="<?php echo site_url(); ?>assets/images/common/bread-list-3.jpg" alt="">
            </div>
            <div class="dish">
                <img class="dish-img" src="<?php echo site_url(); ?>assets/images/common/bread-list-4.jpg" alt="">
            </div>
        </div>
    </div>
    <div id="content">
    <div class="inner">
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url() ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">profile</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php if ($this->session->flashdata('msg')) {?>
            <div class="alert alert-success" id="success-alert">
                <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>
                <strong>Success! </strong>
                <?php echo $this->session->flashdata('msg'); ?>
            </div>
        <?php }?>
        <div class="row">
            <div class="col-md-4 col-sx-12">

                  <!-- Profile Image -->
                  <div class="box box-primary">
                    <div class="box-body box-profile">
                      <img class="profile-user-img img-responsive img-circle" src="<?php echo site_url('/assets/adminlte/') ?>dist/img/avatar6.png" alt="User profile picture">

                      <h3 class="profile-username text-center"><?php echo $data['name'] ?></h3>

                      <p class="text-muted text-center">BPOTech Company</p>

                      <ul class="list-group list-group-unbordered">
                        <li class="list-group-item clearfix">
                          <b>Địa chỉ</b> <a class="pull-right"><?php echo $data['address'] ?></a>
                        </li>
                        <li class="list-group-item">
                          <b>Số điện thoại</b> <a class="pull-right"><?php echo $data['phone'] ?></a>
                        </li>

                      </ul>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->

                  <!-- About Me Box -->
                  <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title">About Me</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <strong><i class="fa fa-book margin-r-5"></i> Companies</strong>

                      <p class="text-muted">
                       BPO Tech Huế
                      </p>

                      <hr>

                      <strong><i class="fa fa-map-marker margin-r-5"></i> Hobbies</strong>

                      <p class="text-muted">Đọc sách, Chơi game , đi du lịch</p>

                      <hr>

                      <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

                      <p>
                        <span class="label label-danger">UI Design</span>
                        <span class="label label-success">Coding</span>
                        <span class="label label-info">Javascript</span>
                        <span class="label label-warning">PHP</span>
                        <span class="label label-primary">java</span>
                      </p>

                      <hr>

                      <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!-- /.box -->
            </div>
            <div class="col-xs-8">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Đỗi mật khẩu</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="frontend/profile/changepassword" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group has-feedback">
                                <input class="form-control" placeholder="Mật khẩu cũ" type="password" name="oldpwd">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input class="form-control" placeholder="Mật khẩu mới" type="password" name="password">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input class="form-control" placeholder="Retype password" type="password" name="repwd">
                                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="save" class="btn btn-primary" value="1">Lưu</button>
                            <input type="hidden" name="usemail" value="<?php echo $data['email'] ?>" />
                        </div>
                    </form>
                </div>
                 <!-- /.change pass -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Cập nhật thông tin</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" action="frontend/profile/update" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group has-feedback">
                                <input class="form-control" placeholder="Họ và tên" id="fullname" type="text" name="name" value="<?php echo $data['name'] ?>">
                                <span class="glyphicon glyphicon-modal-window form-control-feedback"></span>
                                 <span id="note-fullname" style="color: #c00;"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input class="form-control" id="phone" placeholder="Số điện thoại" type="text" name="phone" value="<?php echo $data['phone'] ?>">
                                <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                                 <span id="note-phone" style="color: #c00;"></span>
                            </div><div class="form-group has-feedback">
                                <input class="form-control" id="address" placeholder="Địa chỉ" type="text" name="address" value="<?php echo $data['address'] ?>">
                                <span class="glyphicon glyphicon-home form-control-feedback"></span>
                                 <span id="note-address" style="color: #c00;"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input class="form-control" placeholder="Mật khẩu cũ" type="password" name="password">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input class="form-control" placeholder="Retype password" type="password" name="repwd">
                                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="save" class="btn btn-primary" value="1">Lưu</button>
                            <input type="hidden" name="usid" value="<?php echo $data['id'] ?>" />
                        </div>
                    </form>
                </div>
                <!-- update infomation -->
            </div>
        </div>
    </section>
<!-- /.content -->
</div>
</div>
</div>