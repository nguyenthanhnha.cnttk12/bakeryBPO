
<section id="header">
   <div class="headContent">
            <div class="inner">
                <div class="social-list">
                    <ul class="mainMenu">
                        <li class="item">
                            <a href="https://www.facebook.com" class="link-social"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="item">
                            <a href="https://twitter.com" class="link-social"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="item">
                            <a href="https://plus.google.com" class="link-social"><i class="fa fa-google-plus"></i></a>
                        </li>
                        <li class="item">
                            <a href="https://www.pinterest.com/" class="link-social"><i class="fa fa-pinterest"></i></a>
                        </li>
                        <li class="item">
                            <a href="https://www.linkedin.com/" class="link-social"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div><!-- .headContent -->
        <div class="mainHead">
            <div class="inner clearfix">
                <div class="logoHead">
                    <a href="<?php echo site_url(); ?>" class="logolink">
                        <img src="<?php echo site_url(); ?>assets/images/common/logo-head.png" alt="">
                    </a>
                </div>
                <a href="javascript:void(0);" class="icon" id="btnMenu">
                    <i class="fa fa-bars"></i>
                </a>
                <ul class="menu">
                    <li class="item icon1"><a href="<?php echo site_url(); ?>" class="link-menu-head">Home</a></li>
                    <li class="dropdown">
                        <a class="item icon2 dropdown-toggle" data-toggle="dropdown" style="font-size: 25px; color: #fff;">
                            <i class="fa fa-user"></i>
                        </a>
                        <div class="dropdown-menu fix-drop" style="min-width: 100px;">
                            <a href="/profile" class="">
                                Profile
                            </a>
                            <a href="/login/signout" class="">
                                signout
                            </a>
                        </div>
                    </li>
                    <li class="header-shop item icon3">
                        <a class="btn-shopping-cart" href="<?php echo site_url('cart'); ?>">
                            <i class="fa fa-shopping-cart"></i>
                            <span <?php echo ($this->cart->total_items() > 0) ? "style= 'display: block;'" : "style= 'display: none;'" ?> id="cart-qty-notification" class="cart-qty-notification"><?php echo $this->cart->total_items() ?></span>
                        </a>
                    </li>
                </ul>
            </div><!-- .inner -->
        </div><!-- .mainMenu -->
</section>


