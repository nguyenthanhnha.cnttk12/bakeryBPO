<section id="footer">
   <div class="inner">
            <div class="footInfo">
                <a class="logoFooter hoverJS" href="index.html"><img src="<?php echo site_url(); ?>assets/images/common/logo-head.png" alt="Bread-BPO"></a>
                <p class="textinfo">Hồ Đắc Quyền</p>
                <p class="textinfo">TEL　0932 436 554</p>
                <p class="textinfo">E-mail hodacquyenpx@gmail.com</p>
            </div>
            <!-- <div class="bnFooter">
                <p class="photo"><a class="hoverJS" href="recruit.html"><img src="img/common/banner_footer1.png" alt=""></a></p>
                <p class="photo"><a class="hoverJS" href="#"><img src="img/common/banner_footer2.png" alt=""></a></p>
            </div> -->
        </div><!-- .inner -->

</section>
 <p id="copyright">Copyright © 2018 nguyenthanhnha.cnttk12@gmail.com, All Rights Reserved.</p>


<!-- Modal -->
<div class="modal fade" id="fileManagerModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Bootstrap Modal with Dynamic Content</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript">
    var BASE_URL = '<?php echo base_url(); ?>'
</script>
<script
              src="https://code.jquery.com/jquery-2.2.4.min.js"
              integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
              crossorigin="anonymous"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo site_url('assets/bootstrap/js/bootstrap.min.js') ?>" crossorigin="anonymous"></script>

<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- <script src="<?php echo site_url('assets/bootstrap/js/bootstrap-filestyle.min.js') ?>"></script> -->
<!-- Select2 -->
<script src="<?php echo site_url('assets/js') ?>/plugins/select2/select2.full.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo site_url('assets/js') ?>/plugins/icheck/icheck.min.js"></script>
<!-- Confirmation -->
<script src="<?php echo site_url('assets') ?>/bootstrap/js/bootstrap-confirmation.js"></script>
<script src="<?php echo site_url('assets') ?>/js/jquery.blockUI.js"></script>

<!-- Latest compiled and minified JavaScript -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>         -->
<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="<?php echo site_url('assets/fancybox') ?>/jquery.fancybox.js?v=2.1.5"></script>
<!-- AdminLTE App -->
<script src="<?php echo site_url('assets/adminlte') ?>/dist/js/app.min.js"></script>
<script src="<?php echo site_url('assets/js') ?>/custom.js"></script>

<script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
        $('input[type="checkbox"].normal, input[type="radio"].normal').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue'
                    // ,increaseArea: '20%' // optional
                });

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
        $(document).on('ifChecked', '.checkth, .checkft', function (event) {
            $('.checkth, .checkft').iCheck('check');
            $('.checkitem').each(function () {
                $(this).iCheck('check');
            });
        });
        $(document).on('ifUnchecked', '.checkth, .checkft', function (event) {
            $('.checkth, .checkft').iCheck('uncheck');
            $('.checkitem').each(function () {
                $(this).iCheck('uncheck');
            });
        });
        $(document).on('ifUnchecked', '.checkitem', function (event) {
            $('.checkth, .checkft').attr('checked', false);
            $('.checkth, .checkft').iCheck('update');
        });
    });
</script>
<?php
//display js
$this->carabiner->display('js');
?>

</body>
</html>