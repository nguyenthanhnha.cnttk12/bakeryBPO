<div class="">
	<div class="col-md-6">
		<div class="">
			<div class="cart-alert-message"><i class="fa fa-check-circle"></i> 1 sản phẩm đã được thêm vào giỏ hàng của bạn</div>
		</div>
		<?php
$featured_image = NULL;

foreach ($product->images as $key => $image) {
	if ($image->featured == 'Yes') {
		$featured_image = $image;
		break;
	}
}
?>
		<div class="row">
			<div class="col-md-6"><img src="<?php echo $featured_image->path; ?>"></div>
			<div class="col-md-6">
				<?php foreach ($this->cart->contents() as $items) {?>
				<div class="cart-alert-name"><?php echo $items['name'] ?>
					<br />
                                                <a class="remove-from-cart-ajax" data-rowid="<?php echo $items['rowid'] ?>" title="Xóa khỏi giỏ hàng"><i class="fa fa-trash"></i></a>
				</div>
				<div class="cart-alert-price"><?php echo number_format($items['price'], 0, ',', '.'); ?> ₫</div>
				<div class="input-group">

                   <input name="giavi" class="form-control input-sm text-center giavi<?php echo $items['id'] ?> " value="<?php echo $items['giavi'] ?>" />
                    <a class="input-group-addon AJAX-insert-giavi cart-index-update-giavi" data-target="giavi<?php echo $items['id'] ?>" data-rowid="<?php echo $items['rowid'] ?>" ><i class="fa fa-plus"></i>Thêm gia vị</a>
                </div>
            	<?php }?>
			</div>
		</div>
	</div>
	<div class="col-md-6 cart-alert-cart-info">
		<div class="cart-alert-my-cart">Giỏ hàng của bạn (<?php echo $this->cart->total_items(); ?> sản phẩm) </div>
		<div class="row">
			<div class="col-md-6">Tạm tính</div>
			<div class="col-md-6 text-right"><?php echo number_format($this->cart->total(), 0, ',', '.'); ?> ₫</div>
			<div class="col-md-12"><hr style="margin-bottom: 10px;margin-top: 10px;"></div>
			<div class="col-md-6">Tổng cộng</div>
			<div class="col-md-6 text-right cart-alert-price">
				<?php echo number_format($this->cart->total(), 0, ',', '.'); ?> ₫
				<div>Đã bao gồm VAT (nếu có)</div>

			</div>
		</div>
		<div class="row" style="margin-top: 10px;">
			<div class="col-md-6">
				<a class="btn btn-default" href="<?php echo site_url('cart') ?>">Đến giỏ hàng</a>
			</div>
			<div class="col-md-6 text-right ">
				<a class="btn btn-warning" href="<?php echo site_url('checkout') ?>">Thanh toán</a>
			</div>
		</div>
	</div>
</div>