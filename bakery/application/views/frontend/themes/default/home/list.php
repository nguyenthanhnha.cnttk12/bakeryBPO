<?php foreach ($products as $key => $product) {?>
    <div class="boxItem" itemscope style="<?php echo ($isajax) ? 'display:none' : '' ?>">
        <a class="wrap-box" href="#" itemprop="url" style="text-decoration-line: none;">
            <meta itemprop="category" content="<?php echo $product->category ?>" />
            <meta itemprop="productId" content="<?php echo $product->id ?>" />
            <meta itemprop="image" content="<?php echo site_url($product->image); ?>" />
            <img class="boxImage" src="<?php echo site_url($product->image); ?>">

            <div class="boxContent">
                <h3 class="title-box"><?php echo $product->name ?></h3>
                <meta content="<?php echo number_format($product->price, 0, ',', '.'); ?> ₫" itemprop="price">
                <meta content="VND" itemprop="priceCurrency">
                <meta content="http://schema.org/InStock" itemprop="availability">
                <p style="color: #f92a2a;" class="price-box"><?php echo number_format($product->price, 0, ',', '.'); ?> ₫</p>
            </div>
        </a>
            <p class="wrap-cart">
                <button class="add-to-cart" data-productid="<?php echo $product->id; ?>"><i class="fa fa-shopping-basket" aria-hidden="true"></i>
                </button>
            </p>

        </a>
    </div>
<?php }?>

