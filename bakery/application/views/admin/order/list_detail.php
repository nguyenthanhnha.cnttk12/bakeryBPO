<tr>
                                    <th style="width: 20px"><input type="checkbox" class="minimal checkth" ></th>
                                    <th style="width: 150px">Ảnh</th>
                                    <th>Khách hàng</th>
                                    <th>Số điện thoại</th>
                                    <th>Sản phẩm</th>
                                    <th>Số lượng đặt</th>
                                    <th>Tổng thanh toán</th>
                                    <th>Trạng thái</th>
                                    <th></th>
                                    <th style="width: 80px"></th>
                                </tr>
                                <?php if ($data['total'] > 0) {?>
                                    <?php foreach ($data['products'] as $key => $product) {?>
                                        <tr>
                                            <td><input type="checkbox" class="minimal checkitem" name="val[]" value="<?php echo $product->order_id ?>" ></td>
                                            <td><img id="thumbPreview" class="img-thumbnail" alt="Ảnh sản phẩm" src="<?php echo site_url($product->image) ?>" style="width: 160px;" /></td>
                                            <td><?php echo $product->username ?></td>
                                            <td><?php echo $product->phone ?></td>
                                            <td><?php echo $product->product ?></td>
                                            <td>1</td>
                                            <td><?php echo number_format($product->price, 0, ',', '.'); ?> ₫</td>
                                            <td><?php echo $product->status ?></td>
                                            <td></td>
                                            <td>
                                             <div class="dropdown">
                                              <button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-edit"></i>
                                              </button>
                                              <div class="dropdown-menu fix-drop">
                                                <a href="/admin/order/edit/<?php echo $product->order_id . "/completed"; ?>" class="btn btn-xs btn-primary">Hoàn thành</a>
                                                <a href="/admin/order/edit/<?php echo $product->order_id . "/confirmed"; ?>" class="btn btn-xs btn-primary" >đã xác nhận</a>
                                                <a href="/admin/order/edit/<?php echo $product->order_id . "/pending"; ?>" class="btn btn-xs btn-primary" >chờ xử lý</a>
                                                <a href="/admin/order/edit/<?php echo $product->order_id . "/cancel"; ?>" class="btn btn-xs btn-primary" >Hủy</a>
                                                <a href="/admin/order/delete/<?php echo $product->order_id; ?>" class="btn btn-xs btn-danger" data-toggle="confirmation" data-placement="left" data-singleton="true"><i class="fa fa-trash-o"></i></a>
                                              </div>
                                            </div>



                                            </td>
                                        </tr>
                                    <?php }?>
                                <?php } else {?>
                                    <tr>
                                        <td colspan="3" class="text-center"><?php echo $this->config->item('no_data') ?></td>
                                    </tr>
                                <?php }?>